package com.student.click.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.student.click.studentmanagementsystem.R;
import com.student.click.util.Constant;

public class TaskSelection extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_selection);
        Log.d("Activity", "Started");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_task_selection,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.exit:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void passTaskChoice(View view) {
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.taskChoiceRG);
        if (radioGroup.getCheckedRadioButtonId() != -1) {
            int radioButtonID = radioGroup.getCheckedRadioButtonId();
            RadioButton radioButton = (RadioButton) findViewById(radioButtonID);
            Intent intent = new Intent(this,MainActivity.class);
            String selection = radioButton.getText().toString();
            if (selection.compareTo("Async Task")==0){
                intent.putExtra("task", Constant.ASYNC_TASK);
            }else if(selection.compareTo("Services")==0){
                intent.putExtra("task",Constant.SERVICE);
            }else if(selection.compareTo("Intent Services")==0){
                intent.putExtra("task",Constant.INTENT_SERVICE);
            }
            startActivity(intent);

        }

    }
}
