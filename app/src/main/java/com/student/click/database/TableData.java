package com.student.click.database;


import android.provider.BaseColumns;

public class TableData {
    //Columns
    public TableData() {
    }

    public static abstract class TableInfo implements BaseColumns {
        public static final String STUDENT_NAME = "username";
        public static final String STUDENT_BRANCH = "branch";
        public static final String STUDENT_ROLL = "roll";
        public static final String STUDENT_IMAGE = "image";
        public static final String DATABASE_NAME = "student_info";
        public static final String TABLE_NAME = "reg_info";
    }

}
