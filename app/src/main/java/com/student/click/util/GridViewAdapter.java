package com.student.click.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.student.click.entities.Student;
import com.student.click.studentmanagementsystem.R;

import java.io.File;
import java.util.ArrayList;

public class GridViewAdapter extends BaseAdapter {

    ArrayList<Student> studentList;
    LayoutInflater inflater;
    Context context;


    public GridViewAdapter(Context context, ArrayList<Student> list) {
        this.studentList = list;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return studentList.size();
    }

    @Override
    public Student getItem(int position) {
        return studentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.student_grid_structure, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        Student student = getItem(position);

        String imageName=student.getImage()+".jpg";
        if(imageName.compareTo("profile.jpg")==0){
            int resource = R.mipmap.profile;
            mViewHolder.image.setImageResource(resource);
        }
        else {
            String imagePath = Environment.getExternalStorageDirectory() + File.separator + "StudentMS" + File.separator + "SAVED" + File.separator;
            File image = new File(imagePath + imageName);
            Bitmap bitmap= null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.fromFile(image));
            } catch (Exception e) {
                e.printStackTrace();
            }
            mViewHolder.image.setImageBitmap(bitmap);
        }
        mViewHolder.name.setText(student.getName());
        return convertView;
    }


    private class MyViewHolder {
        TextView name;
        ImageView image;

        public MyViewHolder(View item) {
            image = (ImageView) item.findViewById(R.id.profileImageDisplay);
            name = (TextView) item.findViewById(R.id.studentNameGrid);
        }
    }

    public void setStudentList(ArrayList<Student> studentList) {
        this.studentList = studentList;
    }
}
